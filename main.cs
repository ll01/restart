﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;

namespace restart
{
    using System.Threading;
    class Program
    {
        static void Main(string[] args)
        {
            var jsonString = LoadJson("./programs.restart");
            var programsToDetect = JsonConvert.DeserializeObject<List<RestartProcess>>(jsonString);
            RestartProcess.StartProcesses(programsToDetect);

            do
            {
                Thread.Sleep(3000);
                RestartProcess.CheckAllProssesessAreUp(programsToDetect);
            } while (true);

            Console.WriteLine("Hello World!");
        }

        static string LoadJson(string filePath)
        {
            var jsonAsString = "";
            try
            {
                using (var stream = new StreamReader(filePath))
                {
                    jsonAsString = stream.ReadToEnd();
                }
            }
            catch (System.Exception e)
            {

                throw e;
            }
            return jsonAsString;
        }
    }

}
