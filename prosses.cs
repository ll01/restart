

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace restart
{
    class RestartProcess
    {
        private int m_pid;
        private string m_startCommand;

        private bool m_isUp;

        public int Pid { get => m_pid; set => m_pid = value; }
        public string StartCommand { get => m_startCommand; set => m_startCommand = value; }

        public static void StartProcesses(List<RestartProcess> RestartProcessList)
        {
            var process = new Process();
            foreach (var restart in RestartProcessList)
            {
                restart.Start(process);
            }
        }

        public static void CheckAllProssesessAreUp(List<RestartProcess> RestartProcessList)
        {
            var process = new Process();
            foreach (var restart in RestartProcessList)
            {
                if (!restart.IsProcessIsUp())
                {
                    restart.Start(process);
                    Thread.Sleep(1000);
                    if(! restart.m_isUp) {
                        RestartProcessList.Remove(restart);
                    }
                }
            }
        }

        private bool IsProcessIsUp()
        {
            return Process.GetProcesses().Any(x => x.Id == Pid);
        }

        private void Start(Process process)
        {
            if (m_isUp)
            {
                process.StartInfo.FileName = m_startCommand;
                process.Start();
                try
                {
                    var newPid = process.Id;
                    m_pid = newPid;
                    m_isUp = true;
                }
                catch (System.Exception e)
                {
                    m_isUp = false;
                    Console.WriteLine("faild to start process " +
                     m_startCommand + " failed with error" + e.Message);

                }
            }
        }
    }
}
